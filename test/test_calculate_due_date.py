import datetime
import unittest

from src.calculate_due_date import CalculateDueDate, is_weekday, in_working_hours


class CalculateDueDateTest(unittest.TestCase):

    submit_date_tuesday = datetime.datetime(year=2016, month=10, day=11, hour=14, minute=12)
    friday_20161230_1412 = datetime.datetime(year=2016, month=12, day=30, hour=14, minute=12)

    def test_when_no_turnaround_then_original_date(self):
        assert(in_working_hours(self.submit_date_tuesday))
        assert(is_weekday(self.submit_date_tuesday))
        due_date = CalculateDueDate(submit_date=self.submit_date_tuesday,
                                    turnaround_time_hours=0.0)
        self.assertEqual(self.submit_date_tuesday,
                         due_date)

    def test_when_turnaround_dont_occurs_daychange_then_change_hours(self):
        assert(in_working_hours(self.submit_date_tuesday))
        assert(is_weekday(self.submit_date_tuesday))
        due_date = CalculateDueDate(submit_date=self.submit_date_tuesday,
                                    turnaround_time_hours=1.5)
        self.assertEqual(due_date,
                         datetime.datetime(year=2016, month=10, day=11, hour=15, minute=42))

    def test_when_turnaround_occurs_1_nonweekend_daychange_then_change_day_hours(self):
        assert(in_working_hours(self.submit_date_tuesday))
        assert(is_weekday(self.submit_date_tuesday))
        due_date = CalculateDueDate(submit_date=self.submit_date_tuesday,
                                    turnaround_time_hours=9.5)
        self.assertEqual(due_date,
                         datetime.datetime(year=2016, month=10, day=12, hour=15, minute=42))


    def test_when_turnaround_occurs_3_nonweekend_daychange_then_change_day_hours(self):
        assert(in_working_hours(self.submit_date_tuesday))
        assert(is_weekday(self.submit_date_tuesday))
        due_date = CalculateDueDate(submit_date=self.submit_date_tuesday,
                                    turnaround_time_hours=25.5)
        self.assertEqual(due_date,
                         datetime.datetime(year=2016, month=10, day=14, hour=15, minute=42))


    def test_when_turnaround_occurs_weekend_change_then_account_weekend_properly(self):
        assert(in_working_hours(self.submit_date_tuesday))
        assert(is_weekday(self.submit_date_tuesday))
        due_date = CalculateDueDate(submit_date=self.submit_date_tuesday,
                                    turnaround_time_hours=40)
        self.assertEqual(due_date,
                         datetime.datetime(year=2016, month=10, day=18, hour=14, minute=12))

    def test_when_turnaround_occurs_weekend_and_yearchange_then_still_accounts_properly(self):
        assert(in_working_hours(self.friday_20161230_1412))
        assert(is_weekday(self.friday_20161230_1412))
        due_date = CalculateDueDate(submit_date=self.friday_20161230_1412,
                                    turnaround_time_hours=40)
        self.assertEqual(due_date,
                         datetime.datetime(year=2017, month=1, day=6, hour=14, minute=12))

class IsWeekDayTest(unittest.TestCase):

    def test_when_saturday_then_false(self):
        self.assertFalse(is_weekday(datetime.datetime(year=2016, month=10, day=8,
                                                      hour=14, minute=12)))

    def test_when_sunday_then_false(self):
        self.assertFalse(is_weekday(datetime.datetime(year=2016, month=10, day=9,
                                                      hour=14, minute=12)))

    def test_when_monday_then_true(self):
        self.assertTrue(is_weekday(datetime.datetime(year=2016, month=10, day=10,
                                                      hour=14, minute=12)))