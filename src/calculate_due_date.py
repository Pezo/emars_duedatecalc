import datetime


def CalculateDueDate(submit_date : datetime.datetime, turnaround_time_hours : float):
    turnaround_timedelta = datetime.timedelta(hours=turnaround_time_hours)
    due_date = account_by_days(start_dt=submit_date, accountable_td=turnaround_timedelta)
    return due_date


def account_by_days(start_dt, accountable_td):
    if is_weekday(dt=start_dt):
        date_start = get_working_start_datetime(dt=start_dt)
        date_end   = get_working_end_datetime  (dt=start_dt)
        thisdays_accountable_td = date_end - start_dt
        if accountable_td <= thisdays_accountable_td:
            due_date = start_dt + accountable_td
        else: # recursive account remain
            accountable_remain_td = accountable_td - thisdays_accountable_td
            next_day_start = date_start + datetime.timedelta(days=1)
            if accountable_remain_td < datetime.timedelta(hours=8):
                due_date = next_day_start + accountable_remain_td
            else:
                due_date = account_by_days(start_dt=next_day_start,
                                           accountable_td=accountable_remain_td)
    else:
        start_dt += datetime.timedelta(days=1) # skip day
        due_date = account_by_days(start_dt=start_dt,
                                   accountable_td=accountable_td)
    return due_date


def get_working_end_datetime(dt):
    date_end = datetime.datetime(year=dt.year, month=dt.month, day=dt.day, hour=17)
    return date_end


def get_working_start_datetime(dt):
    date_start = datetime.datetime(year=dt.year, month=dt.month, day=dt.day, hour=9)
    return date_start


def is_weekday(dt : datetime.datetime):
    return False if dt.isoweekday() in [6, 7] else True


def in_working_hours(dt):
    if get_working_start_datetime(dt) < dt < get_working_end_datetime(dt):
        return True
    return False